# OpenML dataset: ESL

https://www.openml.org/d/1035

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

1. Title: Employee Selection (Ordinal ESL)

2. Source Informaion:
Donor: Arie Ben David
MIS, Dept. of Technology Management
Holon Academic Inst. of Technology
52 Golomb St.
Holon 58102
Israel
abendav@hait.ac.il
Owner: Yoav Ganzah
Business Administration School
Tel Aviv Univerity
Ramat Aviv 69978
Israel

3. Past Usage:

4. Relevant Information
The ESL data set contains 488 profiles of applicants for certain industrial
jobs.  Expert psychologists of a recruiting company, based upon psychometric
test results and interviews with the candidates, determined the values of the
input attributes. The output is the an overall score corresponding to the
degree of fitness of the candidate to this type of job.


5. Number of Instances: 488

6. Number of Attributes: 4 input, 1 output.

7. Attribute Information: All input and output values are ORDINAL.

8. Missing Attribute Values: None.

9. Class Distribution:

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/1035) of an [OpenML dataset](https://www.openml.org/d/1035). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/1035/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/1035/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/1035/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

